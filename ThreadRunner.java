
import java.util.concurrent.Semaphore;

public class ThreadRunner {

  public static void main(String args[]) {
    MathCal math = new MathCal();
    Thread evenThread = new Thread(new EvenThread(math));
    Thread oddThread = new Thread(new OddThread(math));
    evenThread.start();
    oddThread.start();
  }

}
