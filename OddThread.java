

public class OddThread implements Runnable {

  MathCal calculator;
  public OddThread(MathCal math) {
    calculator = math;
  }

  public void run() {
    int i = 0;
    while(i < 10) {
      calculator.oddCal(i);
      i++;
    }
  }
}
