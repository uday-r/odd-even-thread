

public class EvenThread implements Runnable {

  MathCal calculator;
  public EvenThread(MathCal math) {
    calculator = math;
  }

  public synchronized void run() {
    int i = 0;
    while(i < 10) {
      calculator.evenCal(i);
      i++;
    }
  }
}
