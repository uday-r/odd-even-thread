
public class MathCal {

  public synchronized void oddCal(int i) {
    notify();
    try {
      wait();
      if(i%2 != 0) {
        System.out.println("Odd  " + i);
      }
    } catch(InterruptedException e) {
      e.printStackTrace();
    }
    notify();
  }

  public synchronized void evenCal(int i) {
    notify();
    try {
      wait();
      if(i%2 == 0) {
        System.out.println("Even " + i);
      }
    } catch(InterruptedException e) {
      e.printStackTrace();
    }
    notify();
  }
}
